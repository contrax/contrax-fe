const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
const webpack = require("webpack");

module.exports = {
  devServer: {
    proxy: {
      "/api": {
        target: "https://timeterm.viasalix.nl/contrax/api/v1",
        changeOrigin: true,
        pathRewrite: {
          "^/api": "/"
        }
      }
    },
    disableHostCheck: true
  },
  chainWebpack: config => {
    const svgRule = config.module.rule("svg");

    svgRule.uses.clear();

    svgRule
      .use("babel-loader")
      .loader("babel-loader")
      .end()
      .use("vue-svg-loader")
      .loader("vue-svg-loader");
  },
  transpileDependencies: ["vuex-module-decorators"],
  publicPath: process.env.DEPLOYMENT_LOCATION === "scaleway" ? "/contrax/" : "/"
};
