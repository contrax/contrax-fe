# OAG is openapi-generator.

OAG_VER = 4.2.3
OAG_IMG = openapitools/openapi-generator-cli:v$(OAG_VER)
OAG_PATH_PREFIX = /local
OAG = docker run --rm -u $(shell id -u ${USER}) -v ${PWD}:$(OAG_PATH_PREFIX) $(OAG_IMG)
OAG_GEN_DIR = src/api
OAG_SPEC = api/openapi.yml
OAG_GENERATOR = typescript-axios

.PHONY: gen-api-sdk
gen-api-sdk:
	$(OAG) generate -i $(OAG_PATH_PREFIX)/$(OAG_SPEC) -g $(OAG_GENERATOR) -o $(OAG_PATH_PREFIX)/$(OAG_GEN_DIR)

.PHONY: image
image:
	docker build .