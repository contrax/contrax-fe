import { Configuration } from "@/api";
import { AxiosInstance } from "axios";

const apiBasePath = process.env.VUE_APP_CONTRAX_API_BASE_PATH ?? "/api";

export function createAPI<T>(
  factory: (
    configuration?: Configuration,
    basePath?: string,
    axios?: AxiosInstance
  ) => T,
  configuration?: Configuration,
  axios?: AxiosInstance
): T {
  return factory(configuration, apiBasePath, axios);
}
