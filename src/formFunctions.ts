export function setElementValidity(elementId: string) {
  let input = document.getElementById(elementId) as HTMLFormElement;
  let output = document.getElementById(elementId + "-validity") as HTMLElement;
  if (output.style.display == "initial") {
    output.style.display = input.checkValidity() ? "none" : "initial";
  }
}

export function setElementValidityAfter(elementId: string) {
  let input = document.getElementById(elementId) as HTMLFormElement;
  let output = document.getElementById(elementId + "-validity") as HTMLElement;
  output.style.display = input.checkValidity() ? "none" : "initial";
}

export function setCurrencyValidityAfter(elementId: string) {
  let input = document.getElementById(elementId) as HTMLFormElement;
  let stringValue = formatCurrencyElementAfter(input.value);
  let output = document.getElementById(elementId + "-validity") as HTMLElement;
  output.style.display =
    !input.checkValidity() || stringValue == "0" || stringValue == "0.00"
      ? "initial"
      : "none";
}

export function resetElementValidity(elementId: string) {
  let element = document.getElementById(elementId) as HTMLElement;
  element.style.display = "none";
}

export function formatCurrencyElement(amount: string) {
  if (!amount) amount = "";
  let stringValue: string = amount.trim();
  return stringValue.replace(/[^0-9,.]/g, "");
}

export function formatCurrencyElementAfter(amount: string) {
  if (!amount) amount = "";
  let stringValue: string = amount.trim();
  stringValue = stringValue.replace(/[,.](\d{1,2})$/, ".$1");
  if (/[,.]\d$/.test(stringValue)) stringValue += "0";
  stringValue = stringValue.replace(/[^0-9.]|[.](?=.*[.]|\d\d\d+)/g, "");
  stringValue = stringValue.replace(/^0*(0.*)/g, "$1");
  return stringValue;
}

export function focusElement(inputElementId: string, focusElementId: string) {
  let input = document.getElementById(inputElementId) as HTMLFormElement;
  setElementValidityAfter(inputElementId);
  if (input.checkValidity()) {
    try {
      let element = document.getElementById(focusElementId) as HTMLElement;
      element.focus();
    } catch (e) {}
  }
}

export function resetFormElement(
  formElementId: string,
  focusElementId: string
) {
  let form = document.getElementById(formElementId) as HTMLFormElement;
  form.reset();
  let element = document.getElementById(focusElementId) as HTMLElement;
  element.focus();
}
