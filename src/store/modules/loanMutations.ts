import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import * as ContraxAPI from "@/api";
import { AxiosError, AxiosResponse } from "axios";
import { createAPI } from "@/apiUtils";

@Module({ dynamic: true, namespaced: true, store, name: "loanMutations" })
class LoanMutations extends VuexModule {
  isLoading = false;
  error: ContraxAPI.ModelError | null = null;
  loanMutations: ContraxAPI.LoanMutation[] = [];

  @Mutation
  SET_IS_LOADING(isLoading: boolean) {
    this.isLoading = isLoading;
  }

  @Mutation
  PUSH_LOAN_MUTATIONS(loanMutations: ContraxAPI.LoanMutation[]) {
    this.loanMutations.push(...loanMutations);
  }

  @Mutation
  CLEAR_LOAN_MUTATIONS() {
    this.loanMutations = [];
  }

  @Mutation
  SET_ERROR(error: ContraxAPI.ModelError) {
    this.error = error;
  }

  @Mutation
  UNSET_ERROR() {
    this.error = null;
  }

  @Action
  async getByLoanId(loanID: number) {
    this.SET_IS_LOADING(true);

    let cursor = undefined;
    let api = createAPI(ContraxAPI.LoanApiFactory) as ContraxAPI.LoanApi;

    try {
      while (true) {
        let loans: AxiosResponse<ContraxAPI.LoanMutations> = await _loadMutationsFromCursor(
          api,
          loanID,
          cursor
        );
        let data = loans.data.data;

        if (data && data.length > 0) {
          if (!cursor) this.CLEAR_LOAN_MUTATIONS();
          this.PUSH_LOAN_MUTATIONS(data);

          if (data.length < 50 || loans.data.nextCursor === undefined) break;
          cursor = loans.data.nextCursor;
        } else break;
      }

      this.UNSET_ERROR();
    } catch (e) {
      const err: AxiosError = e;
      if (err.response) this.SET_ERROR(err.response.data);
    } finally {
      this.SET_IS_LOADING(false);
    }
  }
}

async function _loadMutationsFromCursor(
  api: ContraxAPI.LoanApi,
  loanID: number,
  cursor?: string
): Promise<AxiosResponse<ContraxAPI.LoanMutations>> {
  return api.getLoanMutations(loanID, undefined, undefined, cursor);
}

export default getModule(LoanMutations);
