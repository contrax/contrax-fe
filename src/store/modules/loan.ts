import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import * as ContraxAPI from "@/api";
import { AxiosError } from "axios";
import { createAPI } from "@/apiUtils";

@Module({ dynamic: true, namespaced: true, store, name: "loan" })
class Loan extends VuexModule {
  isLoading = false;
  error: ContraxAPI.ModelError | null = null;
  loan: ContraxAPI.Loan | null = null;

  @Mutation
  SET_IS_LOADING(isLoading: boolean) {
    this.isLoading = isLoading;
  }

  @Mutation
  SET_LOAN(loan: ContraxAPI.Loan) {
    this.loan = loan;
  }

  @Mutation
  UNSET_LOAN() {
    this.loan = null;
  }

  @Mutation
  SET_ERROR(error: ContraxAPI.ModelError) {
    this.error = error;
  }

  @Mutation
  UNSET_ERROR() {
    this.error = null;
  }

  @Action
  async getByID(id: number) {
    this.SET_IS_LOADING(true);

    let api = createAPI(ContraxAPI.LoanApiFactory);

    try {
      let loan = await api.getLoan(id);
      this.SET_LOAN(loan.data);
      this.UNSET_ERROR();
    } catch (e) {
      const err: AxiosError = e;
      if (err.response) this.SET_ERROR(err.response.data);
    } finally {
      this.SET_IS_LOADING(false);
    }
  }
}

export default getModule(Loan);
