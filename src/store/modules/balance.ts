import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import * as ContraxAPI from "@/api";
import { AxiosError } from "axios";
import { createAPI } from "@/apiUtils";

@Module({ dynamic: true, namespaced: true, store, name: "balance" })
class Balance extends VuexModule {
  isLoading = false;
  error: ContraxAPI.ModelError | null = null;
  balance: ContraxAPI.Balance | null = null;

  get balanceSet() {
    return !!this.balance && !this.error;
  }

  @Mutation
  SET_BALANCE(balance: ContraxAPI.Balance) {
    this.balance = balance;
  }

  @Mutation
  SET_IS_LOADING(isLoading: boolean) {
    this.isLoading = isLoading;
  }

  @Mutation
  SET_ERROR(error: ContraxAPI.ModelError) {
    this.error = error;
  }

  @Mutation
  UNSET_ERROR() {
    this.error = null;
  }

  @Action
  async refresh() {
    this.SET_IS_LOADING(true);
    let api = createAPI(ContraxAPI.BalanceApiFactory);
    try {
      let balance = await api.getBalance();
      this.SET_BALANCE(balance.data);
      this.UNSET_ERROR();
    } catch (e) {
      const error: AxiosError = e;
      if (error.response) {
        this.SET_ERROR(error.response.data);
      }
    } finally {
      this.SET_IS_LOADING(false);
    }
  }
}

export default getModule(Balance);
