import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import * as ContraxAPI from "@/api";
import { AxiosError } from "axios";
import { createAPI } from "@/apiUtils";
import * as Rx from "rxjs";
import moment from "moment";

@Module({ dynamic: true, namespaced: true, store, name: "user" })
class User extends VuexModule {
  isLoading = true;
  error: ContraxAPI.ModelError | null = null;
  user: ContraxAPI.User | null = null;

  constructor(module: VuexModule) {
    super(module);

    let expirationString = localStorage.getItem("tokenExpiration");
    if (expirationString) {
      let expirationMoment = moment(expirationString);
      let seconds = expirationMoment.diff(moment(), "seconds");
      if (seconds > 0) {
        _refreshToken(seconds);
      }
    }
  }

  get signedIn() {
    return !!this.user && !this.error;
  }

  @Mutation
  SET_USER(user: ContraxAPI.User) {
    this.user = user;
  }

  @Mutation
  SET_IS_LOADING(isLoading: boolean) {
    this.isLoading = isLoading;
  }

  @Mutation
  SET_ERROR(error: ContraxAPI.ModelError) {
    this.error = error;
  }

  @Mutation
  UNSET_ERROR() {
    this.error = null;
  }

  @Mutation
  UNSET_USER() {
    this.user = null;
  }

  @Action
  async refresh(load = true) {
    if (load) this.SET_IS_LOADING(true);
    let api = createAPI(ContraxAPI.AccountApiFactory);
    try {
      let user = await api.getUser(0);
      this.SET_USER(user.data);
      this.UNSET_ERROR();
    } catch (e) {
      const error: AxiosError = e;
      if (error.response) {
        this.SET_ERROR(error.response.data);
      }
    } finally {
      if (load) this.SET_IS_LOADING(false);
    }
  }

  @Action
  async logOut() {
    this.SET_IS_LOADING(true);
    let api = createAPI(ContraxAPI.AccountApiFactory);
    try {
      await api.logOut();
      this.UNSET_ERROR();
      this.UNSET_USER();
    } finally {
      this.SET_IS_LOADING(false);
    }
  }

  @Action
  async logIn(params: { email: string; password: string; load: boolean }) {
    if (params.load) this.SET_IS_LOADING(true);
    let api = createAPI(ContraxAPI.AccountApiFactory, {
      username: params.email,
      password: params.password
    });
    try {
      let rsp = await api.logIn();

      // No exception thrown, we can safely unset the error.
      this.UNSET_ERROR();

      // Refresh the token automatically and store the expiration.
      _refreshToken(rsp.data.expiresIn);
      _storeTokenExpiration(rsp.data.expiresIn);

      await this.refresh(false);
    } catch (e) {
      this.SET_ERROR(e);
    } finally {
      if (params.load) this.SET_IS_LOADING(false);
    }
  }

  @Action
  async refreshToken() {
    let api = createAPI(ContraxAPI.AccountApiFactory);
    try {
      let rsp = await api.refreshToken();

      _refreshToken(rsp.data.expiresIn);
      _storeTokenExpiration(rsp.data.expiresIn);
    } catch (e) {
      // We don't want this error to clog up the user interface, retry in 5 seconds.
      _refreshTokenAbsolute(10);
    }
  }

  @Action
  async register(account: ContraxAPI.Account) {
    this.SET_IS_LOADING(true);
    let api = createAPI(ContraxAPI.AccountApiFactory);
    try {
      await api.createAccount(account);
      this.UNSET_ERROR();

      await this.logIn({
        email: account.email,
        password: account.password,
        load: false
      });
    } catch (e) {
      this.SET_ERROR(e);
    } finally {
      this.SET_IS_LOADING(false);
    }
  }
}

let _cancelRefreshToken = () => {};

function _refreshToken(expiresInSeconds: number) {
  if (_cancelRefreshToken) _cancelRefreshToken();

  // Refresh 10 hours prior to expiry.
  const offset = 10 * 3600;

  expiresInSeconds -= offset;
  if (expiresInSeconds < 0) expiresInSeconds = 0;

  _refreshTokenAbsolute(expiresInSeconds);
}

function _refreshTokenAbsolute(seconds: number) {
  const source = new Rx.Observable(observer => {
    const id = setTimeout(() => observer.next(), seconds * 1000);
    return () => clearTimeout(id);
  });
  const sub = source.subscribe(() => getModule(User).refreshToken());

  _cancelRefreshToken = () => {
    sub.unsubscribe();
  };
}

function _storeTokenExpiration(expiresInSeconds: number) {
  let expirationDate = moment().add(expiresInSeconds, "seconds");
  localStorage.setItem("tokenExpiration", expirationDate.format());
}

export default getModule(User);
