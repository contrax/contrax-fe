import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import * as ContraxAPI from "@/api";
import { AxiosError, AxiosResponse } from "axios";
import { createAPI } from "@/apiUtils";

@Module({ dynamic: true, namespaced: true, store, name: "loans" })
class Loans extends VuexModule {
  isLoading = false;
  error: ContraxAPI.ModelError | null = null;
  loans: ContraxAPI.Loan[] = [];

  @Mutation
  SET_IS_LOADING(isLoading: boolean) {
    this.isLoading = isLoading;
  }

  @Mutation
  PUSH_LOANS(loans: ContraxAPI.Loan[]) {
    this.loans.push(...loans);
  }

  @Mutation
  CLEAR_LOANS() {
    this.loans = [];
  }

  @Mutation
  SET_ERROR(error: ContraxAPI.ModelError) {
    this.error = error;
  }

  @Mutation
  UNSET_ERROR() {
    this.error = null;
  }

  @Action
  async refresh() {
    this.SET_IS_LOADING(true);

    let cursor = undefined;
    let api = createAPI(ContraxAPI.LoanApiFactory) as ContraxAPI.LoanApi;

    try {
      while (true) {
        let loans: AxiosResponse<ContraxAPI.Loans> = await _loadLoansFromCursor(
          api,
          cursor
        );
        let data = loans.data.data;

        if (data && data.length > 0) {
          if (!cursor) this.CLEAR_LOANS();
          this.PUSH_LOANS(data);

          if (data.length < 50 || loans.data.nextCursor === undefined) {
            break;
          }
          cursor = loans.data.nextCursor;
        } else break;
      }

      this.UNSET_ERROR();
    } catch (e) {
      const err: AxiosError = e;
      if (err.response) this.SET_ERROR(err.response.data);
    } finally {
      this.SET_IS_LOADING(false);
    }
  }
}

async function _loadLoansFromCursor(
  api: ContraxAPI.LoanApi,
  cursor?: string
): Promise<AxiosResponse<ContraxAPI.Loans>> {
  return api.getLoans(
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    cursor
  );
}

export default getModule(Loans);
