export function formatCurrencyNL(num: number) {
  return (
    "€ " +
    num
      .toFixed(2)
      .replace(".", ",")
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
  );
}

export function formatName(firstName: string, lastName: string): string {
  return `${firstName} ${lastName}`;
}
