import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import History from "@/views/History.vue";
import New from "@/views/New.vue";
import SignInRegister from "@/views/SignInRegister.vue";
import PageNotFound from "@/views/PageNotFound.vue";
import EditLoan from "@/views/EditLoan.vue";
import user from "@/store/modules/user";
import Loan from "@/views/Loan.vue";
import LoanMutation from "@/views/LoanMutation.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    meta: { title: "Home", requiresAuth: true },
    component: Home
  },
  {
    path: "/history",
    name: "history",
    meta: { title: "History", requiresAuth: true },
    component: History
  },
  {
    path: "/new",
    name: "new",
    meta: { title: "New Loan", requiresAuth: true },
    component: New
  },
  {
    path: "/signIn",
    name: "signIn",
    meta: { title: "Sign In", requiresAuth: false },
    component: SignInRegister
  },
  {
    path: "/loan/:id",
    name: "loan",
    meta: { title: "Loan", requiresAuth: true },
    component: Loan
  },
  {
    path: "/mutation/:id",
    name: "mutation",
    meta: { title: "Mutation", requiresAuth: true },
    component: LoanMutation
  },
  {
    path: "/loan/:id/edit",
    name: "editLoan",
    meta: { title: "Edit Loan", requiresAuth: true },
    component: EditLoan
  },
  {
    path: "*",
    name: "pageNotFound",
    meta: { title: "Page Not Found", requiresAuth: false },
    component: PageNotFound
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, _from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!user.isLoading && !user.signedIn) {
      next("/signIn");
    } else {
      next();
    }
  } else {
    next();
  }
});

router.beforeEach((to, _from, next) => {
  document.title = `Contrax · ${to.meta.title}`;
  next();
});

export default router;
